import cv2
import numpy as np

image = cv2.imread('girl.jpg')

# Туториал для изучения - https://github.com/wb-08/opencv-tutorial/tree/master

def loading_displaying_saving():
    """ Загрузка, отоборажения и сохранение изображения """
    image = cv2.imread('girl.jpg', cv2.IMREAD_GRAYSCALE)
    cv2.imshow('Title: Girl', image)
    cv2.waitKey(0)
    cv2.imwrite('test.jpg', image)


def accessing_and_manipulating():
    """ Доступ к пикселям и манипулирование ими """
    image = cv2.imread('girl.jpg')
    (b, g, r) = image[0][0]
    image[0, 0] = (255, 0, 0)
    (b, g, r) = image[0, 0]


def resizing(width: int, height: int, interp=cv2.INTER_LINEAR):
    """ Изменение размеров изображения """

    h, w = image.shape[:2]
    if width is None and height is None:
        return image

    if width is None:
        dimension = int((width * (height/h)), height)
    else:
        dimension = (width, int(height * (width/w)))

    return cv2.resize(image, dimension, interpolation=interp)


def shifting():
    """ ... """
    h, w = image.shape[:2]
    translation_matrix = np.float32([[1, 0, 500], [0, 1, 600]])
    dst = cv2.warpAffine(image, translation_matrix, (w, h))
    new = cv2.imwrite('photo_new.jpg', dst)
    cv2.imshow('photo_new.jpg', new)
    cv2.waitKey(0)

shifting()