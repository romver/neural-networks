import random
from pprint import pprint
def train_lang_model(file):
    """ Считывает текст из файла и разделяет на слова"""
    with open(file, 'r', encoding='utf-8') as f:
        text = f.read()
    words = text.split()

    # Создаем словарь для хранения цепей Маркова
    markov_chain = {}
    prev_word = None

    # Обучение модели - создаем цепи Маркова для каждого слова

    for word in words:
        if prev_word is not None:
            if prev_word not in markov_chain:
                markov_chain[prev_word] = []
            markov_chain[prev_word].append(word)
        prev_word = word
    return markov_chain


def generate_sentence(markov_chain: dict, length=10):
    """ Генерация предложения на основе обученной модели """

    if not markov_chain:
        return "Модель не обучена"
    start_word = random.choice(list(markov_chain.keys()))
    sentence = [start_word]

    for item in range(length - 1):
        next_word = random.choice(markov_chain[sentence[-1]])
        sentence.append(next_word)
    result = ' '.join(sentence).capitalize()
    return result


# Обучение модели
lang_model = train_lang_model('text.txt')

# Генерация предложения из 15 слов
generated_sentence = generate_sentence(lang_model, length=15)
pprint(generated_sentence)
